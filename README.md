# LiDAR
Resources to work with the Velodyne LiDAR unit on the Husky robot


So far only a notebook has been written to visualise one frame of a PCD file, which has been exported through ROS tools. To try it out, make sure PDAL 1.8.0 is installed in Anaconda through the conda-forge repository. The environment is included also.

1. Download Anaconda

2. Import the environment

   ``conda env create -f environment.yml``

3. Run Jupyter notebook

   ``jupyter notebook``

4. Load velodyne_visualise.ipynb



## Velodyne lidar python library

The library found at [Github](https://github.com/esrlabs/velodyne) interprets the pcap files of a HDL64E S2 and outputs them into a msgpack file.

As the ethernet packet is the same for both the HDL32 and above, it has been relatively easy to adapt this source code. The problems I've faced are:

* There are 64 LiDAR scanners on the HDL64E S2 vs 32 on ours, for some reason I've found no errors with this. Should come back to this. My thoughts are:
  * There are just double the amount of points. In the visualisation, if I look at what is considered one revolution ~100,000 points, there are clearly around 2 or more revolutions of data
  * Perhaps there are the same amount of receivers. 
* For some reason when the rotation angle is exactly 360.0, the program finds a KeyError. This may be with the calibration file, or a problem converting the data. At the moment I just skip any instances of this rotation angle with an if statement, so we miss a point here and there. 
* There are no GPS UDP packets in the HDL64E S2 apparently, so the program was written without this in mind. The GPS packets are 512 bits (+42 bits ethernet header) on the HDL32E. 
  * I will need to write something to obtain this data if we will use it. It would look fairly straight foward. At the moment, there's an if statement that will skip any data packets if they are under 512 bits. 
  * Alternatively, if we can use the RTK receiver on the robot, we may be able to time this anyway. I believe the GPS timestamp is still included on the HDL64E, and the packet that comes through is just a NMEA sequence, we can just produce our own NMEA sequence with the better GPS.
* The program will put all points into a MSGPACK file. Which for the tunnel file (~90mb and 23,542,144 points) turned out to be around 680mb, much smaller than thousands of 8mb ASCII files.
* These aren't sequenced into timestamps, they're only points

### How to run

This works for me on Python 3.7.1

Follow the README file included inside the Velodyne folder, or on the original github page. Make sure to download the dependencies. 



[The calibration file from VeloView](https://github.com/Kitware/VeloView/blob/master/share/HDL-32.xml) was used and has already been converted to hdl32e.json. Substitute this as the calibration json file.

Convert the pcap to msgpack

```python
python read_points.py <.pcap file> <msgpack output file> <calibration json file>
```

Then to visualise. The start and end point indices should be around 0 and 50000 for one revolution it seems

```python
python visualize.py msgpack <msgpack file> <start point index> <end point index>
```

![Screenshot_20190203_214244](assets/Screenshot_20190203_214244.png)
